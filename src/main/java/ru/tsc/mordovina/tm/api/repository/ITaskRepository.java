package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAll();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    boolean existsById(String id);

    boolean existsByIndex(int index);

}
