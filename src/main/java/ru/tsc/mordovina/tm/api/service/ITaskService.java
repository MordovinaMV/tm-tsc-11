package ru.tsc.mordovina.tm.api.service;

import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    void create(String name);

    void create(String name, String description);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

}
