package ru.tsc.mordovina.tm.api.controller;

public interface ITaskController {

    void showList();

    void showById();

    void showByIndex();

    void showByName();

    void createTask();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateByIndex();

    void updateById();

    void showTasks();

    void clearTasks();

}
