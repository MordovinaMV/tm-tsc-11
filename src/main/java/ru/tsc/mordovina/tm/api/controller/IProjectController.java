package ru.tsc.mordovina.tm.api.controller;

import ru.tsc.mordovina.tm.model.Project;

public interface IProjectController {

    void showList();

    void showById();

    void showByIndex();

    void showByName();

    void createProject();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateByIndex();

    void updateById();

    void showProjects();

    void clearProjects();

}
